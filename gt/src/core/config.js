import firebase from "firebase/app";
import firestore from "firebase/firestore"
import 'firebase/storage'
import { getStorage } from "firebase/storage"

const firebaseConfig = {
  apiKey: "AIzaSyC1CjJBZkpSSdLUsN5pHtrooGjQgg4a-hI",
  authDomain: "gtapp-c7c93.firebaseapp.com",
  projectId: "gtapp-c7c93",
  storageBucket: "gtapp-c7c93.appspot.com",
  messagingSenderId: "192450439973",
  appId: "1:192450439973:web:4ea25b047367d5d77688cc"
};

if (! firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}
firebase.firestore();

const storage = firebase.storage();

export {firebase, storage};