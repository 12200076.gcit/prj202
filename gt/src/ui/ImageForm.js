import {Text,View,StyleSheet,TextInput, Image,KeyboardAvoidingView, SafeAreaView,TouchableOpacity, source, } from 'react-native'
import React, { useState } from 'react'
import { firebase } from '../core/config'
import { theme } from '../core/theme'
import Button from '../components/button'
import { Picker } from '@react-native-picker/picker'
export default function ImageForm(props, {navigation}) {
  const [loading, setLoading] = useState();
  const [title, setTitle] = useState('')
  const [phonenumber, setPhoneNumber] = useState('')
  const [category, setCategory] = useState('java')
  const [year, setYear] = useState('')
  const Name='item-'+ new Date().toISOString();
  
  const uploadImage = async() => {
    setLoading(true)
    const uri = props.route.params.image;
    const childPath = 'items/'
    const response = await fetch(uri);
    const blob = await response.blob();
    const task = firebase
      .storage()
      .ref()
      .child(childPath+Name)
      .put(blob);
      if (title === '' || phonenumber === '' || year === '' ){
          alert('field cannot be empty')
          setLoading(false)
          return false;
      }
      else {
    const taskProgress = snapshot => {
      console.log('transferred: ${snapshot.bytesTransferred}')
    }
    const taskCompleted = ()=> {
      task.snapshot.ref.getDownloadURL().then((snapshot) => {
        savePostData(snapshot);
      })
    }
    const taskError = snapshot => {
      console.log(snapshot)
    }
    task.on("state_changed", taskProgress, taskError, taskCompleted);
  }
  const savePostData =  (downloadURL) => {
    firebase.firestore()
      .collection('post')
      .add({
        title,
        phonenumber,
        category,
        year,
        downloadURL,
        creation: firebase.firestore.FieldValue.serverTimestamp()
      })
      .then((function () {
        alert('Successfully uploaded')
        props.navigation.navigate('HomeScreen')
        setLoading(false)
    }))
  };
}
  return (

    <View style={{ flex: 1, paddingTop: 15, alignItems: 'center'}}>
      <Image style={{width: '50%', height: '30%'}} source={{uri: props.route.params.image}}/>
      <Text>{'\n'}</Text>
      <View style={style.form}>
        <TextInput
          style={style.input}
          require={source}
          value={title}
          onChangeText={(text)=> setTitle(text)}
          placeholder='Title'
          />
          <TextInput
            value={phonenumber}
            onChangeText={(text)=> setPhoneNumber(text)}
            style={style.input}
            require={source}
            maxLength= {8}
            placeholder='Phone Number'/>



          <Picker
          style={style.picker}
          selectedValue={category}
          require={source}
          mode='dropdown'
          onValueChange={(itemValue) =>
            setCategory(itemValue)}
          >
            <Picker.Item label="Year" value="Year" />
            <Picker.Item label="Year 1" value="Year 1" />
            <Picker.Item label="Year 2" value="Year 2" />
            <Picker.Item label="Year 3" value="Year 3" />
            <Picker.Item label="Year 4" value="Year 4" />
            <Picker.Item label="Others" value="other" />
          </Picker>
          <TextInput
            value={year}
            onChangeText={(text)=> setYear(text)}
            style={style.input}
            placeholder='Description'
            />
      
      <View style={{width: '20%', alignItems: 'center', backgroundColor: '#566AF5',
          borderRadius: 8,
          borderColor: 'red',
          width: 135,
          height: 50,
          marginLeft: '18%',
          marginTop:'10%',
          marginBottom:"5%",
          justifyContent:'center'
          }}>
        <TouchableOpacity
          style={styles.const}
          mode='contained'
          loading={loading}
          require={source}
          onPress={() => uploadImage()}><Text style={{color: "white", fontSize: 20 }} >Add</Text></TouchableOpacity>
      </View>
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  imageBack: {
    width: 25,
    height: 25,
},
const:{
  height:"60%",
  

}
})
const style = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    backgroundColor:'#fff',
  },
  inputCon:{
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
      maxHeight: "20%",
    maxWidth: "100%",
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
  },
  picker: {
    height: 50,
    width: 250,
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
    backgroundColor: theme.colors.item
  },
  input2:{
    height: 50,
    width: 150,
    margin: 12,
    borderBottomWidth: 1,
    padding: 10,
  },
  form:{
    marginBottom: "20%",
    borderRadius:20,
    borderColor:'black',
    backgroundColor:"lightblue",
    maxWidth:"100%",
    textAlign:"center",
    marginLeft:"5%",
  },
})
