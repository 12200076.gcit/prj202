import { View, Text, TouchableOpacity, StyleSheet, Image, ScrollView, FlatList, Pressable } from 'react-native'
import React, {useState, useEffect} from 'react'
import Button from '../components/button'

import { Camera } from 'expo-camera'
import * as ImagePicker from 'expo-image-picker'
import { SafeAreaView } from 'react-native-safe-area-context';
import { theme } from '../core/theme'

const GTImagePicker = ({navigation}) => {
  const [hasGalleryPermission, setHasGalleryPermission] = useState(null);
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [camera, setCamera] = useState(null);
  const [image, setImage] = useState(null);
  const [type, setType] = useState(Camera.Constants.Type.back)

  useEffect(() => {
    (async () => {
      const cameraStatus = await Camera.requestCameraPermissionsAsync();
      setHasCameraPermission(cameraStatus.status === 'granted');

      const galleryStatus = await ImagePicker.requestCameraPermissionsAsync();
      setHasGalleryPermission(galleryStatus.status === 'granted');

    })();
  }, []);

  const takePicture = async () => {
    if(camera) {
      const data = await camera.takePictureAsync(null);
      setImage(data.uri);
    }
  }

  const pickImage = async () => {
    let result  = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect : [1, 1],
      quality: 1,
    });
    console.log(result);
    
    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  if (hasCameraPermission === false || hasGalleryPermission === false) {
    return <View />;
  }
  if (hasCameraPermission === false || hasGalleryPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <SafeAreaView style={styles.safeArea}>
        <View style={{ flex: .8 }}>
          <View style={styles.cameraContainer}>
            <Camera 
            ref={ref => setCamera(ref)}
            style={styles.fixedRatio} 
            type={type}
            ratio={'4:3'} />
          </View>

          <TouchableOpacity
            style={{ 
              flex: 0.3,
              alignItems: 'center',
              backgroundColor: '#566AF5',
              borderRadius: 8,
              width: "40%",
              marginLeft: '30%',
              marginTop:'5%',
              justifyContent:'center'
            }}
            color={theme.colors.start}
            title='Flip Image'
            onPress={() => {
              setType(
                type === Camera.Constants.Type.back
                  ? Camera.Constants.Type.front
                  : Camera.Constants.Type.back
              );
            }}
            >
              <Text style={{color: "white", }}>Flip Image</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => takePicture()}>
            <Text style={{textDecorationLine:'underline', color: 'blue', textAlign:'center',fontSize: 15}}>{'\n'}TAKE PICTURE{'\n'}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => pickImage()}>
            <Text style={{textDecorationLine:'underline', color: 'blue', textAlign:'center', fontSize: 15}}>PICK IMAGE FROM GALLERY{'\n'}</Text>
          </TouchableOpacity>
          
          {image && <Image source={{uri: image}} style={{ flex: 1 }} />}
        </View>
        <View style={{alignItems: 'center', marginTop:10,}}>
          <TouchableOpacity
          style={styles.btnstyle} 
          onPress={()=> navigation.navigate("ImageForm", {image})} title='Choose' color={theme.colors.start} ><Text style={{textAlign: "center", color:"white"}}>Choose</Text></TouchableOpacity>
          <Text>{'\n'}</Text>
        </View>
      </SafeAreaView>
  )
}

export default GTImagePicker;

const styles = StyleSheet.create({
  cameraContainer: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 1
  },
  fixedRatio: {
    aspectRatio: 1
  },
  safeArea: {
    flex: 1,
    padding: 5,
    maxHeight: '100%'
  },
  btnstyle: {
    backgroundColor: '#566AF5',
    borderRadius: 8,
    borderColor: 'red',
    width: "40%",
    height: "20%",
    marginTop:'20%',
    justifyContent:'center'

  }
})