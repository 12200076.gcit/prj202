
import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import {SliderBox} from "react-native-image-slider-box";



export default class ImageSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [ 
    
        
        require('../../assets/image1.png'),
        require('../../assets/image2.png'),
        require('../../assets/image3.png'),


        
        
      ]
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <SliderBox
          style={{width:'100%',height:270}}
          autoplay={true}
          circleLoop={true}
          images={this.state.images}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0,
    width: "100%",
    height: "35%",
  }
});
