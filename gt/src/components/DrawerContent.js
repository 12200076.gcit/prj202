import React from "react";
import { View, StyleSheet } from "react-native";
import { DrawerItem, DrawerContentScrollView } from "@react-navigation/drawer";
import {
    Avatar,
    Drawer,
    Switch,
    Text,
    Title,
    TouchableRipple,
} from "react-native-paper";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
export default function Drawerjs(props) {
    return (
        <DrawerContentScrollView {...props}>
            <View style={styles.drawerContent}>
                <View style={styles.userInfoSection}>
                    <Avatar.Image
                        size={90}
                        source={
                            require("../../assets/gtlogoo.png")
                        }
                    />
                </View>
                <Drawer.Section style={styles.drawerSection}>
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Icon
                                name="home-outline"
                                color={color}
                                size={size}
                            />
                        )}
                        label="Home"
                        onPress={() => { props.navigation.navigate('HomeScreen') }}
                    />
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Icon
                                name="post-outline"
                                color={color}
                                size={size}
                            />
                        )}
                        label="Post"
                        onPress={() => { props.navigation.navigate('PostScreen') }}
                    />
                    <DrawerItem
                        icon={({ color, size }) => (
                            <Icon
                                name="about-outline"
                                color={color}
                                size={size}
                            />
                        )}
                        label="About Us"
                        onPress={() => { props.navigation.navigate('AboutUs') }}
                    />
                </Drawer.Section>
            </View>
        </DrawerContentScrollView>
    )
}
const styles = StyleSheet.create({
    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 100,
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    drawerSection: {
        marginTop: 15,
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },
})







