// import { View, Text, TouchableOpacity,Button, StyleSheet, Image, ScrollView, FlatList, Pressable } from 'react-native'
// import React, {useState, useEffect, useCallback} from 'react'
// // import {firebase} from '../core/config'
// // import firebase from "firebase";
// import * as DocumentPicker from 'expo-document-picker';
// import { Camera } from 'expo-camera'
// import * as ImagePicker from 'expo-image-picker'
// import { SafeAreaView } from 'react-native-safe-area-context';
// import { theme } from '../core/theme'

// const GTDocumentPicker = ({navigation}) => {
//   const [hasGalleryPermission, setHasGalleryPermission] = useState(null);
//   const [hasCameraPermission, setHasCameraPermission] = useState(null);
//   const [camera, setCamera] = useState(null);
//   const [image, setImage] = useState([]);
//   const [type, setType] = useState(Camera.Constants.Type.back)

//   useEffect(() => {
//     (async () => {
//       const cameraStatus = await Camera.requestCameraPermissionsAsync();
//       setHasCameraPermission(cameraStatus.status === 'granted');

//       const galleryStatus = await DocumentPicker.requestCameraPermissionsAsync();
//       setHasGalleryPermission(galleryStatus.status === 'granted');

//     })();
//   }, []);

//   const takePicture = async () => {
//     if(camera) {
//       const data = await camera.takePictureAsync(null);
//       // console.log(data.uri)
//       setImage(data.uri);
//     }
//   }

// //   const pickImage = async () => {
// //     let result  = await ImagePicker.launchImageLibraryAsync({
// //       mediaTypes: ImagePicker.MediaTypeOptions.All,
// //       allowsEditing: true,
// //       aspect : [1, 1],
// //       quality: 1,
// //     });
// //     setImage(result);
// //     console.log(
// //         result.name,
// //         result.size,
// //         result.uri
// //     );
    
// //     // if (!result.cancelled) {
// //     //   setImage(result.uri);
// //     // }
// //   };
//       const [fileResponse, setFileResponse] = useState([]);
      
//         const handleDocumentSelection = useCallback(async () => {
//           try{
//             const response = await DocumentPicker.getDocumentAsync({
//               presentationStyle: 'pageScreen',
//             });
//             setFileResponse(response);
//             console.log(
//               response.uri,
//               response.name,
//               response.size,
//               response.file
//             )  
//           } catch (err){
//             console.warn(err);
//           };
//           if (!response.cancelled) {
//               setFileResponse(response.uri);
//           }
//         }, []);

//   if (hasCameraPermission === false || hasGalleryPermission === false) {
//     return <View />;
//   }
//   if (hasCameraPermission === false || hasGalleryPermission === false) {
//     return <Text>No access to camera</Text>;
//   }

//   return (
//     <SafeAreaView style={styles.safeArea}>
//         <View style={{ flex: .8 }}>
//           <View style={styles.cameraContainer}>
//             <Camera 
//             ref={ref => setCamera(ref)}
//             style={styles.fixedRatio} 
//             type={type}
//             ratio={'4:3'} />
//           </View>

//           <Button
//             style={{ 
//               flex: 0.1,
//               alignSelf: 'flex-end',
//               alignItems: 'center',
//             }}
//             color={theme.colors.start}
//             title='Flip Image'
//             onPress={() => {
//               setType(
//                 type === Camera.Constants.Type.back
//                   ? Camera.Constants.Type.front
//                   : Camera.Constants.Type.back
//               );
//             }}>
//           </Button>
//           <TouchableOpacity onPress={() => takePicture()}>
//             <Text style={{textDecorationLine:'underline', color: 'blue', textAlign:'center',fontSize: 15}}>{'\n'}TAKE PICTURE{'\n'}</Text>
//           </TouchableOpacity>
//           {/* <TouchableOpacity onPress={() => pickImage()}> */}
//           <TouchableOpacity onPress={() => handleDocumentSelection ()}>
//             <Text style={{textDecorationLine:'underline', color: 'blue', textAlign:'center', fontSize: 15}}>PICK IMAGE FROM GALLERY{'\n'}</Text>
//           </TouchableOpacity>
//              {/* {fileResponse.map((file, index) => (
// //                 <Text
// //                 key={index.toString()}
// //                 style={styles.uri}
// //                 numberOfLines={1}
// //                 ellipsizeMode={'middle'}>
// //                 {file?.uri}
// //                 </Text>
// //             ))}   */}
//               <Text style={styles.textStyle}>
//                File Name: {fileResponse.name ? fileResponse.name : ''}
//                {'\n'}
//                File Size: {fileResponse.size ? fileResponse.size : ''}
//                {'\n'}
//                File Size: {fileResponse.uri ? fileResponse.uri : ''}
//                {'\n'}
//              </Text>
//              {fileResponse && <View source={{uri: fileResponse}} style={{ flex: 1 }} />}
//           {/* {image && <Image source={{uri: image}} style={{ flex: 1 }} />} */}
//         </View>
//         <View style={{alignItems: 'center', marginTop:10}}>
//           <Button onPress={()=> navigation.navigate("SoftcopyScreen", {fileResponse})} title='Choose' color={theme.colors.start} />
//           <Text>{'\n'}</Text>
//         </View>
//       </SafeAreaView>
//   )
// }

// export default GTDocumentPicker;

// const styles = StyleSheet.create({
//   cameraContainer: {
//     flex: 1,
//     justifyContent: 'center',
//     flexDirection: 'row',
//     borderRadius: 10,
//     borderWidth: 1
//   },
//   fixedRatio: {
//     aspectRatio: 1
//   },
//   safeArea: {
//     flex: 1,
//     padding: 5,
//     maxHeight: '90%'
//   }
// })