import React, {useState, useEffect,Component} from 'react';
import {View ,Text, SafeAreaView, Image,StyleSheet, StatusBar, Dimensions, Linking, Alert} from 'react-native'
import {theme} from '../core/theme'
import { TextInput, FlatList, TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import {firebase} from '../core/config'
import {getStorage, ref, getDownloadURL} from 'firebase/storage'
import CheckBox from '../components/CheckBox';
import { Feather,FontAwesome} from '@expo/vector-icons';

const width = Dimensions.get("screen").width/2-30

export default function ThirdYear({navigation}){
    const [catergoryIndex, setCategoryIndex] = React.useState(0);
    const categories = []
    const [users, setUsers] = useState([])

    const [music, setMusic] = useState(false);

    const todoRef = firebase.firestore().collection('post').where("category", "==", "Year 3")
    useEffect(()=>{
        todoRef
        .onSnapshot(
          querySnapshot  => {
            const users = []
            
                  querySnapshot.forEach((doc)=> {
                    const {title, phonenumber, downloadURL, category, year} = doc.data()
                    users.push({
                      id: doc.id,
                      title,
                      phonenumber,
                      downloadURL,
                      category,
                      year,
                    })
                  })
                  setUsers(users)
                } 
        )
    }, [])
    const CategoryList = () => {
        return (
        <View style={style.categoryContainer}>
            {categories.map((item, index) => (
            <TouchableOpacity
                key={index}
                activeOpacity={0.8}
                onPress={() => setCategoryIndex(index)}>
                <Text
                style={[
                    style.categoryText,
                    catergoryIndex === index && style.categoryTextSelected,
                ]}>
                {item}
                </Text>
            </TouchableOpacity>
            ))}
        </View>
        );
        
    };
    
const renderItem=({item})=>{
 const makeCall = () => {

        let phoneNumber ='';
    
        if (Platform.OS === 'android') {
            phoneNumber = `tel:${item.phonenumber}`;
        } else {
            phoneNumber =`telprompt:${item.phonenumber}`;
        }
    
        Linking.openURL(phoneNumber);
    };
     
        return(
            <View>
                <View style={style.Cart}>
                    <View style={{ alignItems: 'center'}}></View>
                        <Image
                            style={{width: "100%", height: 160}}
                            source={{uri: item.downloadURL}}/>
                        <View style={{paddingTop: 25}}>
                        <Text style={{fontWeight: 'bold', fontSize: 14,}}
                        >
                        Title:{item.title}
                        </Text>
                    </View>
                        <TouchableOpacity onPress={makeCall}>
                                {/* <Text><Feather name="phone-call" size={24} color="green" />{item.phonenumber}</Text> */}
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                       
                        <Text style={{
                            fontSize: 13,
                        }}
                        > <Feather  name="phone-call" size={24} color="green" /> {item.phonenumber}</Text>
                       

                        <View style={{
                            height: 20,
                            width: 20,
                            flexDirection: 'row',
                            backgroundColor: theme.colors.start,
                            borderRadius: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                            
                        }}>
                            <Text style={{fontSize: 15,color: 'white', fontWeight: 'bold'}}>+</Text>
                        </View>
                    </View>
                        </TouchableOpacity>
                        <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        <Text style={{
                            fontSize: 13,
                        }}
                        >Description: {item.year}</Text>
                        <View style={{
                            height: 20,
                            width: 20,
                            backgroundColor: theme.colors.start,
                            borderRadius: 5,
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                            <Text style={{fontSize: 15,color: 'white', fontWeight: 'bold'}}>+</Text>
                        </View>
                    </View>

                </View>    
            </View>
        )
    }
    return (
        <SafeAreaView
            style={{
                flex: 1,
                paddingHorizontal: 20,
                backgroundColor: 'white',
            }}
        >
        <StatusBar
        barStyle='light-content'
        backgroundColor={theme.colors.start}
        />
        <CategoryList/>

          <FlatList
            data={users}
            keyExtractor={item => item.id}
            numColumns={1}
            renderItem={renderItem}
            />
        </SafeAreaView> 
    )
}
const style = StyleSheet.create({
    header: {
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    searchContainer: {
        height: 40,
        borderRadius: 10,
        flex: 1,
        backgroundColor: theme.colors.item,
        flexDirection: 'row',
        alignItems: 'center',
    },
    input: {
        fontSize: 18,
        fontWeight: 'bold',
        flex: 1,
        color: 'black',
      },
    sortBtn: {
        marginLeft: 10,
        height: 40,
        width: 40,
        borderRadius: 10,
        backgroundColor: theme.colors.item,
        justifyContent: 'center',
        alignItems: 'center',
      },
      
      categoryContainer: {
        flexDirection: 'row',
        marginTop: 15,
        marginBottom: 20,
        justifyContent: 'space-between',
      },
      categoryText: {fontSize: 16, color: 'grey', fontWeight: 'bold'},
      categoryTextSelected: {
        color: theme.colors.start,
        paddingBottom: 5,
        borderBottomWidth: 2,
        borderColor: theme.colors.start,
      },
      Cart: {
          backgroundColor: theme.colors.item,
          marginHorizontal: 2,
          borderRadius: 10,
          marginBottom: 20,
          width: "100%",
          backgroundColor:"lightblue",
      }
})

