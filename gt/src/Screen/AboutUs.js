import React from "react";
import Header from "../components/header";
import Background from "../components/Background";
import Backbutton from "../components/Backbutton";
import {Text, StyleSheet, Image, View, TouchableOpacity, Linking } from 'react-native'
import { ScrollView } from "react-native-gesture-handler";
import { Feather } from "@expo/vector-icons";

export default function AboutUs({navigation}){


    const makeCall =() => {
        let phonenumber = '';

        if (Platform.OS === 'android') {
            phonenumber = 'tel: 17388308';
        } else {
            // phonenumber = 'telprompt:0123456789';
            phonenumber = '12200076.gcit@rub.edu.bt';
        }

        Linking.canOpenURL(phonenumber)
        .then(supported => {
            if (!supported){
                Alert.alert("Phone number is not available");
            } else {
                return Linking.openURL(phonenumber);
            }
        })
        .catch(err => console.log(err));
    }
    const pemaCall =() => {
        let phonenumber = '';

        if (Platform.OS === 'android') {
            phonenumber = 'tel: 77620394';
        } else {
            // phonenumber = 'telprompt:0123456789';
            phonenumber = '12200076.gcit@rub.edu.bt';
        }

        Linking.canOpenURL(phonenumber)
        .then(supported => {
            if (!supported){
                Alert.alert("Phone number is not available");
            } else {
                return Linking.openURL(phonenumber);
            }
        })
        .catch(err => console.log(err));
    }
    const sangaCall =() => {
        let phonenumber = '';

        if (Platform.OS === 'android') {
            phonenumber = 'tel: 77336789';
        } else {
            // phonenumber = 'telprompt:0123456789';
            phonenumber = '12200076.gcit@rub.edu.bt';
        }

        Linking.canOpenURL(phonenumber)
        .then(supported => {
            if (!supported){
                Alert.alert("Phone number is not available");
            } else {
                return Linking.openURL(phonenumber);
            }
        })
        .catch(err => console.log(err));
    }
    const tsheringlCall =() => {
        let phonenumber = '';

        if (Platform.OS === 'android') {
            phonenumber = 'tel: 17346805';
        } else {
            // phonenumber = 'telprompt:0123456789';
            phonenumber = '12200076.gcit@rub.edu.bt';
        }

        Linking.canOpenURL(phonenumber)
        .then(supported => {
            if (!supported){
                Alert.alert("Phone number is not available");
            } else {
                return Linking.openURL(phonenumber);
            }
        })
        .catch(err => console.log(err));
    }
    const tsheringCall =() => {
        let phonenumber = '';

        if (Platform.OS === 'android') {
            phonenumber = 'tel: 77878096';
        } else {
            // phonenumber = 'telprompt:0123456789';
            phonenumber = '12200076.gcit@rub.edu.bt';
        }

        Linking.canOpenURL(phonenumber)
        .then(supported => {
            if (!supported){
                Alert.alert("Phone number is not available");
            } else {
                return Linking.openURL(phonenumber);
            }
        })
        .catch(err => console.log(err));
    }
    const karmaCall =() => {
        let phonenumber = '';

        if (Platform.OS === 'android') {
            phonenumber = 'tel: 17571470';
        } else {
            // phonenumber = 'telprompt:0123456789';
            phonenumber = '12200076.gcit@rub.edu.bt';
        }

        Linking.canOpenURL(phonenumber)
        .then(supported => {
            if (!supported){
                Alert.alert("Phone number is not available");
            } else {
                return Linking.openURL(phonenumber);
            }
        })
        .catch(err => console.log(err));
    }
    const sonamCall =() => {
        let phonenumber = '';

        if (Platform.OS === 'android') {
            phonenumber = 'tel: 17391134';
        } else {
            // phonenumber = 'telprompt:0123456789';
            phonenumber = '12200076.gcit@rub.edu.bt';
        }

        Linking.canOpenURL(phonenumber)
        .then(supported => {
            if (!supported){
                Alert.alert("Phone number is not available");
            } else {
                return Linking.openURL(phonenumber);
            }
        })
        .catch(err => console.log(err));
    }








    return (
        <View style={styles.container}>
            <View style={styles.text}>
                <Text>GT-app was built under the aim of serving as a bridge between the senior and junior students.
                    The following are the developer of the app.
                </Text>
            </View>
            <View>
                <View style={styles.ItemBox}>
                    <Image source={require('../../assets/year/pema1.png')} resizeMode='cover' style={{ width: 162, height: 100 }} />
                    <Text>Name: Pema Chophel</Text>
                    <Text>Role: Developer </Text>
                    <TouchableOpacity
                   >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        <Text style={{
                            fontSize: 13,
                        }}
                        ><Feather onPress={() => pemaCall()} name="phone-call" size={24} color="green" />
                        </Text>
                        
                    </View>
                    </TouchableOpacity> 
                </View>
                <View style={styles.ItemBox}>
                    <Image source={require('../../assets/year/tenzin.png')} resizeMode='cover' style={{ width: 162, height: 100, }} />
                    <Text>Name: Sanga Tenzin</Text>
                    <Text>Role: Developer </Text>
                    <TouchableOpacity
                   >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        <Text style={{
                            fontSize: 13,
                        }}
                        ><Feather onPress={() => sangaCall()} name="phone-call" size={24} color="green" />
                        </Text>
                        
                    </View>
                    </TouchableOpacity> 
                </View>
                
                <View style={styles.ItemBox}>
                    <Image source={require('../../assets/year/tshering.png')} resizeMode='cover' style={{ width: 162, height: 100, }} />
                    <Text>Name: Tshering Lhamo</Text>
                    <Text>Role: Project Guide</Text>
                    <TouchableOpacity
                   >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        <Text style={{
                            fontSize: 13,
                        }}
                        ><Feather onPress={() => tsheringlCall()} name="phone-call" size={24} color="green" />
                        </Text>
                        
                    </View>
                    </TouchableOpacity> 
                </View>
                <View style={styles.ItemBox1}>
                    <Image source={require('../../assets/year/sonam.png')} resizeMode='cover' style={{ width: 170, height: 100, }} />
                    <Text> Name: Sonam Wangmo </Text>
                    <Text>Role: Module Tutor</Text>
                    <TouchableOpacity
                   >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        <Text style={{
                            fontSize: 13,
                        }}
                        ><Feather onPress={() => sonamCall()} name="phone-call" size={24} color="green" />
                        </Text>
                        
                    </View>
                    </TouchableOpacity> 
                </View>
                <View style={styles.ItemBox1}>
                    <Image source={require('../../assets/year/karma.png')} resizeMode='cover' style={{ width: 170, height: 100, }} />
                    <Text>Name: Karma Nima</Text>
                    <Text>Role: Developer</Text>
                    <TouchableOpacity
                   >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        <Text style={{
                            fontSize: 13,
                        }}
                        ><Feather onPress={() => karmaCall()} name="phone-call" size={24} color="green" />
                        </Text>
                        
                    </View>
                    </TouchableOpacity> 
                </View>
                
                <View style={styles.ItemBox1}>
                    <Image source={require('../../assets/year/dekar.png')} resizeMode='cover' style={{ width: 170, height: 100, }} />
                    <Text>Name: Tshering Dekar</Text>
                    <Text>Role: Developer</Text>
                    <TouchableOpacity
                   >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        <Text style={{
                            fontSize: 13,
                        }}
                        ><Feather onPress={() => tsheringCall()} name="phone-call" size={24} color="green" />
                        </Text>
                        
                    </View>
                    </TouchableOpacity> 
                </View>
                <View style={styles.contact}>
                    <Text>Contact us: </Text>
                    <TouchableOpacity
                   >
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 5,
                        }}>
                        {/* <Feather onPress={() => makeCall} name="phone-call" size={24} color="green" /> */}
                        <Text style={{
                            fontSize: 13,
                        }}
                        ><Feather onPress={() => makeCall()} name="phone-call" size={24} color="green" />
                        {/* <Text>{"\n"} {"\n"}</Text>
                        <Feather onPress={() => makeCall()} name="email" size={24} color="green" /> */}
                        </Text>
                        
                    </View>
                    </TouchableOpacity>   
                    <Text> Email: all.gcit@.edu.bt</Text>
                    <Text> Contact: 17388308</Text>
                </View>
                
            </View>
            
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        
        
    },
    ItemBox: {
        width: "40%",
        height: 180,
        marginHorizontal: 15,
        marginVertical: 5,
        borderWidth: 1,
        borderRadius: 5,
        shadowColor: 'pink',
        shadowOpacity: 0.99,
        backgroundColor: '#E9EDFE'
    },
    ItemBox1: {
        width: "42%",
        height: 180,
        marginHorizontal: 200,
        marginVertical: -185,
        borderWidth: 1,
        borderRadius: 5,
        shadowColor: 'pink',
        shadowOpacity: 0.99,
        backgroundColor: '#E9EDFE'
    },
    text: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
        width: 340,
        marginHorizontal: 16,
    },
    contact: {
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: "140%",
        width: 340,
        marginHorizontal: 16,
    },
})