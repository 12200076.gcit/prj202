export { default as HomeScreen } from "./HomeScreen";
export { default as PostScreen} from "./PostScreen";
export { default as FirstYear} from "./FirstYear";
export { default as SecondYear} from "./SecondYear";
export { default as ThirdYear} from "./ThirdYear";
export { default as FourthYear} from "./FourthYear";
export { default as Softcopy} from "./Softcopy";
export {default as SoftcopyScreen} from './SoftcopyScreen'
export {default as HardcopyScreen} from './HardcopyScreen'
export {default as GTImagePicker} from '../ui/GTImagePicker'
export {default as ImageForm} from '../ui/ImageForm'
export {default as CheckBox} from '../components/CheckBox'
export {default as AboutUs} from './AboutUs'





