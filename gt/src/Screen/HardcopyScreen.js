
import React, {useState} from "react";
// import { Button } from "react-native-paper";
import { StyleSheet, View, Image,Text } from "react-native";
import Button from "../components/button";
import Header from "../components/header";
import TextInput from "../components/TextInput";
import Background from "../components/Background";
import Backbutton from "../components/Backbutton";
import * as ImagePicker from 'expo-image-picker'
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';


export default function HardcopyScreen({navigation}){

    const [image, setImage] = useState(null);
    const pickImage = async () => {
      // No permissions request is necessary for launching the image library
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
  
      console.log(result);
  
      if (!result.cancelled) {
        setImage(result.uri);
      }
    };

    const data = [
        { label: 'Year 1', value: '1' },
        { label: 'Year 2', value: '2' },
        { label: 'Year 3', value: '3' },
        { label: 'Year 4', value: '4' },
      //   { label: 'Item 5', value: '5' },
      //   { label: 'Item 6', value: '6' },
      //   { label: 'Item 7', value: '7' },
      //   { label: 'Item 8', value: '8' },
      ];
      
      
        const [value, setValue] = useState(null);
        const [isFocus, setIsFocus] = useState(false);
      
        const renderLabel = () => {
          if (value || isFocus) {
            return (
              <Text style={[styles.label, isFocus && { color: 'blue' }]}>
                Dropdown label
              </Text>
            );
          }
          return null;
        };

    return (
        <View style={styles.container}>
            
            <Backbutton goBack={navigation.goBack}/>
            {/* <Header> Hard copy</Header> */}
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',  }}>
                <Button onPress={pickImage} >Select a Photo </Button>
                {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
            </View>

            <View>
                <Text>Phonenumber</Text>
            <TextInput 
                // name="phonenumber"
                validators={['required', 'isNumber','maxNumber:8']}
                errorMessages={['Phonenumber is required', 'Phonenumber invalid' , 'Not a valid number ']}
                // placeholder="Phonenumber"
                // value={phonenumber}
                // onChangeText={(phonenumber) => this.setState({phonenumber})}
                // label="Phonenumber"
                // value={email.value}
                // error={email.error}
                // errorText={email.error}
                // onChangeText={(text) => setEmail ({ value: text, error: ""})}
            />
            </View>

            <View>
                <Text>Tittle</Text>
            <TextInput
                // label="Tittle of the resources"
                // value={tittle.value}
                // error={tittle.error}
                // errorText={tittle.error}
                // onChangeText={(text) => setTittle ({ value: text, error: ""})}
                // secureTextEntry
            />
            </View>
            {/* <View>
                <Text>Year of Student</Text>
            <TextInput
                // label="Year of the student"
                // value={year.value}
                // error={year.error}
                // errorText={year.error}
                // onChangeText={(text) => setYear ({ value: text, error: ""})}
                // secureTextEntry
            />
            </View>  */}
            <View style={styles.container}>
                {/* {renderLabel()} */}
                <Text>Year of Student</Text>
                <Dropdown
                    // value={tittle.value}
                    // error={tittle.error}
                    // errorText={tittle.error}
                    // onChangeText={(text) => setTittle ({ value: text, error: ""})}
                    // secureTextEntry

                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select year' : '...'}
                    searchPlaceholder="Search..."
                    value={value}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={text => {
                    setValue(text.value);
                    setIsFocus(false);
                    }}
                    renderLeftIcon={() => (
                    <AntDesign
                        style={styles.icon}
                        color={isFocus ? 'blue' : 'black'}
                        name="Safety"
                        size={20}
                    />
                    )}
                />
                </View>
            <Button mode={"contained"} onPress = {() => {
                }}>Post</Button>
                
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        textAlign:"center",
        justifyContent:"center",
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
      },
      icon: {
        marginRight: 5,
      },
      label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
      },
      placeholderStyle: {
        fontSize: 16,
      },
      selectedTextStyle: {
        fontSize: 16,
      },
      iconStyle: {
        width: 20,
        height: 20,
      },
      inputSearchStyle: {
        height: 40,
        fontSize: 16,
      },
})