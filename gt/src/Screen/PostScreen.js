import React from "react";
import Button from "../components/button";
import Header from "../components/header";
import { StyleSheet, View } from "react-native";
import Background from "../components/Background";

export default function PostScreen({navigation}){
    return (
        <View style={styles.ViewMain}>
            <View style={styles.viewStyle} android_ripple={{color: '#640233'}}>
            <Button style={{backgroundColor:'#566AF5', width:'50%'}}
                mode="contained"
                onPress = {() => {
                navigation.navigate("GTImagePicker")
                }}
                android_ripple={{color: '#640233'}} > Notes </Button>
                </View>         
        </View>
    )
}
const styles = StyleSheet.create({
    ViewMain:{
        flex:1,
    },
    viewStyle:{
        flex:1,
        justifyContent:"center",
        alignItems:"center",
        backgroundColor:"#E9EDFE",
        marginBottom:'150%',
        width:"100%",
        top:300 ,
        
    
    }
    
    
})