// import {Text,View,StyleSheet,TextInput, Image,KeyboardAvoidingView, SafeAreaView,TouchableOpacity, source} from 'react-native'
// import React, { useState } from 'react'
// import { firebase } from '../core/config'
// import { theme } from '../core/theme'
// import Button from '../components/button'
// // import Icon from 'react-native-vector-icons/MaterialIcons'
// // import Picker from 'react-native-picker'
// import { Picker } from '@react-native-picker/picker'

// export default function SoftcopyScreen(props, {navigation}) {
//   const [loading, setLoading] = useState();
//   const [title, setTitle] = useState('')
//   const [phonenumber, setPhoneNumber] = useState('')
//   const [category, setCategory] = useState('java')
//   const [year, setYear] = useState('')
//   const Name='item-'+ new Date().toISOString();
  
//   const uploadImage = async() => {
//     setLoading(true)
//     const uri = props.route.params.fileResponse;
//     const childPath = 'softcopys/'
//     const response = await fetch(uri);
//     const blob = await response.blob();
//     const task = firebase
//       .storage()
//       .ref()
//       .child(childPath+Name)
//       .put(blob);
//       if (title === '' || phonenumber === '' || year === '' ){
//           alert('field cannot be empty')
//           setLoading(false)
//           return false;
//       }
//       else {
//     const taskProgress = snapshot => {
//       console.log('transferred: ${snapshot.bytesTransferred}')
//     }
//     const taskCompleted = ()=> {
//       task.snapshot.ref.getDownloadURL().then((snapshot) => {
//         savePostData(snapshot);
//       })
//     }
//     const taskError = snapshot => {
//       console.log(snapshot)
//     }
//     task.on("state_changed", taskProgress, taskError, taskCompleted);
//   }
//   const savePostData =  (downloadURL) => {
//     firebase.firestore()
//       .collection('post')
//     //   .doc(firebase.auth().currentUser.uid)
//     //   .collection('items')
//       .add({
//         title,
//         phonenumber,
//         category,
//         year,
//         downloadURL,
//         creation: firebase.firestore.FieldValue.serverTimestamp()
//       })
//       .then((function () {
//         alert('Successfully uploaded')
//         props.navigation.navigate('Softcopy')
//         setLoading(false)
//     }))
//   }
// }
//   return (

//     <View style={{ flex: 1, paddingTop: 15, alignItems: 'center'}}>
//       <View style={{width: '50%', height: '30%'}} source={{uri: props.route.params.fileResponse}}/>
//       <Text>{'\n'}</Text>
//       <View style={style.form}>
//         {/* <View style={styles.con}>  */}
//         {/* <Text>Name/Title</Text> */}
//         <TextInput
//           style={style.input}
//           require={source}
//           value={title}
//           onChangeText={(text)=> setTitle(text)}
//           placeholder='Name/Title'
//           />
//           {/* </View>  */}
//           <TextInput
//             value={phonenumber}
//             onChangeText={(text)=> setPhoneNumber(text)}
//             style={style.input}
//             require={source}
//             placeholder='Phone Number'/>
//           <Picker
//           style={style.picker}
//           selectedValue={category}
//           require={source}
//           mode='dropdown'
//           onValueChange={(itemValue) =>
//             setCategory(itemValue)}
//           >
//             <Picker.Item label="Year 1" value="year 1" />
//             <Picker.Item label="Year 2" value="year 2" />
//             <Picker.Item label="Year 3" value="Year 3" />
//             <Picker.Item label="Year 4" value="Year 4" />
//             <Picker.Item label="Others" value="other" />
//           </Picker>
//           <TextInput
//             value={year}
//             onChangeText={(text)=> setYear(text)}
//             style={style.input}
//             placeholder='year'
//             />
      
//       <View style={{width: '20%', alignItems: 'center',}}>
//         <Button
//           style={styles.const}
//           mode='contained'
//           loading={loading}
//           require={source}
//           onPress={() => uploadImage()}>ADD</Button>
//       </View>
//       </View>
//     </View>
//   )
// }
// const styles = StyleSheet.create({
//   imageBack: {
//     width: 25,
//     height: 25,
// },
// })
// const style = StyleSheet.create({
//   container:{
//     flex: 1,
//     alignItems: 'center',
//     backgroundColor:'#fff',
//   },
//   inputCon:{
//     marginTop: 20,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   input: {
//       maxHeight: "20%",
//     // height: 50,
//     maxWidth: "100%",
//     // width: 250,
//     margin: 12,
//     borderBottomWidth: 1,
//     padding: 10,
//   },
//   picker: {
//     height: 50,
//     width: 250,
//     margin: 12,
//     borderBottomWidth: 1,
//     padding: 10,
//     backgroundColor: theme.colors.item
//   },
//   input2:{
//     height: 50,
//     width: 150,
//     margin: 12,
//     borderBottomWidth: 1,
//     padding: 10,
//   },
// //   con:{
// //     maxWidth:"80%",
// //     marginLeft: "10%"
// //   },
//   const:{
//     maxWidth:"50%",
//     marginLeft: "25%",
//     backgroundColor:"#3da3e0",
//     marginRight:"50%"
//   },
//   form:{
//     marginBottom: "20%",
//     borderRadius:20,
//     borderColor:'black',
//     backgroundColor:"lightblue",
//     // borderColor:"blue",
//     maxWidth:"90%",
//     textAlign:"center",
//     marginLeft:"5%",
//   },
// //   main:{
// //       backgroundColor: "lightblue",
// //       flex: 2,
// //   }
// })

























































import { useState } from "react";
import { StatusBar } from "react-native";
import React,{ useCallback } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import Button from "../components/button";
import Header from "../components/header";
import TextInput from "../components/TextInput";
import { tittleValidator } from "../core/helpers/tittleValidator";
import { yearValidator } from "../core/helpers/yearValidator";
import { emailValidator } from "../core/helpers/emailValidator";
import Backbutton from "../components/Backbutton";
// import DocumentPicker from 'react-native-document-picker';
import * as DocumentPicker from 'expo-document-picker';
import { Dropdown } from 'react-native-element-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign';


export default function SoftcopyScreen({navigation}){
    const [email, setEmail] = useState({value:"", error: ""})
    const [tittle, setTittle] = useState({value:"", error: ""})
    const [year, setYear] = useState({value:"", error: ""})
    // const [loading, setLoading] =useState();

    const onPostPressed = async() => {
        // const emailError = email.value ? "" : "Email can't be empty.";
        const tittleError = tittleValidator(tittle.value);
        const yearError = yearValidator(year.value);
        const emailError = emailValidator(year.value);
        if (emailError || tittleError || yearError){
            setEmail({...email, error: emailError});
            setTittle({...tittle, error: tittleError});
            setYear({...year, error: yearError});
        }
    }
    const [fileResponse, setFileResponse] = useState([]);
      
        const handleDocumentSelection = useCallback(async () => {
          try{
            const response = await DocumentPicker.getDocumentAsync({
              presentationStyle: 'pageScreen',
            });
            setFileResponse(response);
            console.log(
              response.uri,
              response.name,
              response.size,
              response.file
            )  
          } catch (err){
            console.warn(err);
          }
        }, []);
    
    const data = [
  { label: 'Year 1', value: '1' },
  { label: 'Year 2', value: '2' },
  { label: 'Year 3', value: '3' },
  { label: 'Year 4', value: '4' },
//   { label: 'Item 5', value: '5' },
//   { label: 'Item 6', value: '6' },
//   { label: 'Item 7', value: '7' },
//   { label: 'Item 8', value: '8' },
];


  const [value, setValue] = useState(null);
  const [isFocus, setIsFocus] = useState(false);

  const renderLabel = () => {
    if (value || isFocus) {
      return (
        <Text style={[styles.label, isFocus && { color: 'blue' }]}>
          Dropdown label
        </Text>
      );
    }
    return null;
  };
 
  

    return (
        <View style={styles.container}>
            <Backbutton goBack={navigation.goBack}/>
            {/* <Header style={styles.head}> soft copy</Header> */}
            <View style={{flex: 1, justifyContent: "center", alignItems:"center"}}>
            <StatusBar barStyle={'dark-content'} />
            {/* {fileResponse.map((file, index) => (
                <Text
                key={index.toString()}
                style={styles.uri}
                numberOfLines={1}
                ellipsizeMode={'middle'}>
                {file?.uri}
                </Text>
            ))}   */}
            <Text style={styles.textStyle}>
              File Name: {fileResponse.name ? fileResponse.name : ''}
              {'\n'}
              File Size: {fileResponse.size ? fileResponse.size : ''}
              {'\n'}
            </Text>
            <TouchableOpacity
                
                style={{ borderRaduis: 10, padding: 10, width:"80%", alignItems:"center", backgroundColor:"#dQdb3"}}>
                <Button onPress={handleDocumentSelection}>Select files</Button>
            </TouchableOpacity>
            </View>
            <View>
                <Text>Email</Text>
                    <TextInput
                        // label="Email"
                        value={email.value}
                        error={email.error}
                        errorText={email.error}
                        onChangeText={(text) => setEmail ({ value: text, error: ""})}
                    />
            </View>
            <View>
                <Text>Title</Text>
            <TextInput
                // label="Tittle of the resources"
                value={tittle.value}
                error={tittle.error}
                errorText={tittle.error}
                onChangeText={(text) => setTittle ({ value: text, error: ""})}
                // secureTextEntry
            />
            </View>

            {/* <View>
                <Text>Year of Student</Text>
            <TextInput
                // label="Year of the student"
                value={year.value}
                error={year.error}
                errorText={year.error}
                onChangeText={(text) => setYear ({ value: text, error: ""})}
                secureTextEntry
            />
            </View> */}
            <View style={styles.container}>
                {/* {renderLabel()} */}
                <Text>Year of Student</Text>
                <Dropdown
                    // value={tittle.value}
                    error={tittle.error}
                    errorText={tittle.error}
                    onChangeText={(text) => setTittle ({ value: text, error: ""})}
                    secureTextEntry

                    style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    iconStyle={styles.iconStyle}
                    data={data}
                    search
                    maxHeight={300}
                    labelField="label"
                    valueField="value"
                    placeholder={!isFocus ? 'Select year' : '...'}
                    searchPlaceholder="Search..."
                    value={value}
                    onFocus={() => setIsFocus(true)}
                    onBlur={() => setIsFocus(false)}
                    onChange={text => {
                    setValue(text.value);
                    setIsFocus(false);
                    }}
                    renderLeftIcon={() => (
                    <AntDesign
                        style={styles.icon}
                        color={isFocus ? 'blue' : 'black'}
                        name="Safety"
                        size={20}
                    />
                    )}
                />
                </View>
            <Button  mode="contained" onPress = {onPostPressed}>Post</Button>
                
        </View>
    );
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        textAlign:"center",
        justifyContent:"center",
    },
    head:{
        alignItems:'center',
        justifyContent:'center',
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
      },
      icon: {
        marginRight: 5,
      },
      label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
      },
      placeholderStyle: {
        fontSize: 16,
      },
      selectedTextStyle: {
        fontSize: 16,
      },
      iconStyle: {
        width: 20,
        height: 20,
      },
      inputSearchStyle: {
        height: 40,
        fontSize: 16,
      },
})