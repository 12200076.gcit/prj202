import React from "react";
import Header from "../components/header";
import Background from "../components/Background";
import Button from "../components/button";
import Slide from "../components/Slide";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Image

} from 'react-native';
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";

export default function HomeScreen({ navigation }) {


  return (
    <View style={styles.container}>
      <Slide></Slide>
      <View>
        <View style={styles.ItemBox}>
          <Image source={require('../../assets/year/year1a.png')} resizeMode='cover' style={{maxWidth: "100%", height: 100 }} />
          <TouchableOpacity
            style={styles.btnstyle}
            onPress={() => navigation.navigate('FirstYear')}
          >
            <Text style={{ color: 'white', textAlign: 'center' }}>Notes </Text>
          </TouchableOpacity>
        </View>
        
       

        <View style={styles.ItemBox}>
          <Image source={require('../../assets/year/year3a.png')} resizeMode='cover' style={{ maxWidth:"100%", height: 100, }} />
          <TouchableOpacity
            style={styles.btnstyle}
            onPress={() => navigation.navigate('ThirdYear')}
          >
            <Text style={{ color: 'white', textAlign: 'center' }}>Notes</Text>

          </TouchableOpacity>
        </View>

        <View style={styles.ItemBox1}>
          <Image source={require('../../assets/year/year4a.png')} resizeMode='cover' style={{ maxWidth:"100%", height: 100, }} />
          <TouchableOpacity
            style={styles.btnstyle}
            onPress={() => navigation.navigate('FourthYear')}
          >
            <Text style={{ color: 'white', textAlign: 'center' }}>Notes</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.ItemBox1}>
          <Image source={require('../../assets/year/year2a.png')} resizeMode='cover' style={{ maxWidth:"100%", height: 100, }} />
          <TouchableOpacity
            style={styles.btnstyle}
            onPress={() => navigation.navigate('SecondYear')}
          >
            <Text style={{ color: 'white', textAlign: 'center' }}>Notes</Text>
          </TouchableOpacity>
        </View>
      </View>

    </View>

  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  ItemBox: {
    width: "40%",
    height: 200,
    marginHorizontal: 15,
    marginVertical: 10,
    borderWidth: 1,
    borderRadius: 5,
    shadowColor: 'pink',
    shadowOpacity: 0.99,
    backgroundColor: '#E9EDFE'
  },
  ItemBox1: {
    width: "40%",
    height: 200,
    marginHorizontal: "55%",
    marginVertical: -210,
    borderWidth: 1,
    borderRadius: 5,
    shadowColor: 'pink',
    shadowOpacity: 0.99,
    backgroundColor: '#E9EDFE',
  },
  btnstyle: {
    backgroundColor: '#566AF5',
    borderRadius: 8,
    borderColor: 'red',
    width: "90%",
    height: "40%",
    marginLeft: '6%',
    marginTop:'20%',
    justifyContent:'center',

  }

});
