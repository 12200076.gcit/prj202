import React, { Component, useState } from 'react';
import "react-native-gesture-handler"
import { StyleSheet, Text, View, Image, Pressable, Modal, Alert, ScrollView, } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/components/button';
import TextInput from './src/components/TextInput';
import Header from './src/components/header';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen, PostScreen, Softcopy, SecondYear, ThirdYear, SoftcopyScreen, HardcopyScreen, GTImagePicker, ImageForm, AboutUs, FourthYear, FirstYear} from './src/Screen';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Drawerjs from './src/components/DrawerContent';



const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <Provider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName='HomeScreen'
          screenOptions={{ headerShown: false }}>
          <Stack.Screen name='HomeScreen' component={DrawerNavigator} />
          <Stack.Screen name='FirstYear' component={DrawerNavigator} />
          <Stack.Screen name='SecondYear' component={DrawerNavigator} />
          <Stack.Screen name='ThirdYear' component={DrawerNavigator} />
          <Stack.Screen name='FourthYear' component={DrawerNavigator} />
          <Stack.Screen name='Softcopy' component={DrawerNavigator} />
          <Stack.Screen name='SoftcopyScreen' component={DrawerNavigator} />
          <Stack.Screen name='HardcopyScreen' component={DrawerNavigator} />
          <Stack.Screen name='GTImagePicker' component={DrawerNavigator} />
          <Stack.Screen name='ImageForm' component={DrawerNavigator} />
          <Stack.Screen name='AboutUs' component={DrawerNavigator} />

        </Stack.Navigator>
      </NavigationContainer>

    </Provider>
  );
}

function BottomNavigation({navigation}) {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <>

              <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                    }}
                >
                    <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Form Fill up!</Text>  
                        <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress = {() => {
                          navigation.navigate("GTImagePicker"),
                          setModalVisible(!modalVisible)
                          
                          }}
                        >
                        <Text style={styles.textStyle}>Notes</Text>
                        </Pressable>
                        <Text>{''}</Text>
                    </View>
                    </View>
              </Modal> 
    <Tab.Navigator style={styles.tabColor}
     screenOptions={{
      tabBarLabelStyle: { fontSize: 12 },
      tabBarItemStyle: { width: 100 },
      tabBarStyle: {backgroundColor: 'powderblue' },
    }}
    >
      <Tab.Screen
        name='Home'
        component={HomeScreen}
        options={{
          headerShown: false,
          tabBarIcon: ({ size }) => {
            return (
              <Image
                style={{ width: size, height: size }}
                source={
                  require('./assets/home.png')}
              />
            );
          },

        }}
      />
      <Tab.Screen name='Post' component={PostScreen}
        options={{
          tabBarIcon: ({ size }) => {
            return (
              <>
              <Pressable
              name='Post'
              onPress={() =>setModalVisible(true)}
              >
                <Image
                style={{ width: size, height: size }}
                source={
                  require('./assets/post.jpg')}
              />
              </Pressable>  
              
              </>
            );
          },
          headerShown: false
        }}

      />

    </Tab.Navigator>
  </>  
  )
}


const DrawerNavigator = () => {
  return (
    <Drawer.Navigator 
    screenOptions={{
      headerStyle: {
        // backgroundColor: '#FF8524',
        backgroundColor: '#6fdaf7'
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      }, 
    }}
      drawerContent={Drawerjs}>
      <Drawer.Screen name='GT' component={BottomNavigation} />
      <Drawer.Screen name="HomeScreen" component={BottomNavigation} />
      <Drawer.Screen name="PostScreen" component={PostScreen} />
      <Drawer.Screen name="FirstYear" component={FirstYear} />
      <Drawer.Screen name="SecondYear" component={SecondYear} />
      <Drawer.Screen name="ThirdYear" component={ThirdYear} />
      <Drawer.Screen name="FourthYear" component={FourthYear} />
      <Drawer.Screen name="HardcopyScreen" component={HardcopyScreen} />
      <Drawer.Screen name="GTImagePicker" component={GTImagePicker} />
      <Drawer.Screen name="ImageForm" component={ImageForm} />
      <Drawer.Screen name="AboutUs" component={AboutUs} />
      
    </Drawer.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabColor: {
    color: '#3561F5',
  },

  modalView: {
    margin: 20,
    marginTop: "80%",
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    width:"70%"
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
    // backgroundColor: '#566AF5',
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
    
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});
