# PRJ202

GT(give and take) app


GT-app: It is an acronym for Give and Take where the students of GCIT can use it to pass their notes to the other students who are in need of that particular note. There are no logins and registration which makes it easy for the students to use our app freely without having to go through the trouble of registering and logging in. Students can directly open the app and choose the book they want to get and call the number given. As for the soft copy, the students can go to the soft copy section and download the files they need. Other than that, the giver can upload the files and book images so the taker can find it easily.

Built using: React native and Firebase.

Showcase video: https://youtu.be/NPidhNVyL8Q

Poster:
![ALT](/gt/assets/poster/poster.png)

